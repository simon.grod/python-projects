import sys

# Fibonacci Function

def fibonacci(number): 
    i = 0

    if( number == 0): # Fibonacci 0 is always 0
        return 0
    elif (number == 1 or number == 2): # Fibonacci 1 and 2 are always 1
        return 1
    else:
        for i in range(0,number): # Using a recursive function, it will need it's 'old' values, until it reaches the fibonacci number
            return (fibonacci(number-1) + fibonacci(number-2) ) 


# Fibonacci sequency

while(1):
    number = input("Enter the fibonacci number:")
    number = int(number)
    result = fibonacci(number)
    print('The value of the fibonacci asked is: ',result)
    
    while(1):
        yes_or_no=input('Do you wish to calculate another fibonacci? yes/no: ')
        yes_or_no=yes_or_no.lower()
        if (yes_or_no == 'yes'):
            break
        elif (yes_or_no == 'no'):
            sys.exit()
        else:
            print('invalid input, try again')



