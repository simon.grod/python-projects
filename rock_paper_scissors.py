
import random # Random module to generate numbers
import sys # Module used to stop a program

options = ['rock', 'paper','scissors'] # These are computer's options

score_pc = 0
score_user = 0

while(1):

    print("Please select your option:") #User has to select his move
    user=input()
    user=user.lower() # So it won't matter if there's caps in user's input

    if user != "rock" and user != "paper" and user != "scissors": # Bad writing is not accepted
        print("Only 'rock', 'paper' or 'scissors' are options...")
    else:
        option = random.randint(0,2) #Selects a random number so computer's option will always change
        print("Computer selected:",options[option])

        #Shows all the conditional cases so either Computer or User can win
        #Comparing user's option with computer's random option

        if options[option] == user:
            print("There's a tie")
        elif options[option] =='rock':
            if user =='scissors':
                print('Computer Wins')
                score_pc=score_pc+1
            elif user =='paper':
                print('User Wins')
                score_user=score_user+1
        elif options[option] =='paper':
            if user =='scissors':
                print('User Wins')
                score_user=score_user+1
            elif user =='rock':
                print('Computer Wins')
                score_pc=score_pc+1
        elif options[option] =='scissors':
            if user =='paper':
                print('Computer Wins')
                score_pc=score_pc+1
            elif user =='rock':
                print('User Wins')
                score_user=score_user+1

        # Shows score of the game
         
        print('Score: ',score_user, 'User vs ',score_pc, 'Computer')        
        
        # Aks the user for another round, or exit de program

        while(1):
        
            yes_no=input("End of the game! Do you want another try? Select yes or no:")
            yes_no=yes_no.lower()

            if yes_no == 'yes':
                break   
            elif yes_no == 'no':
                sys.exit()
            else: 
                print("Invalid option")


        
