
# Menu Calculator program

import sys

while(1):

    # First is created a dictionary in order to put all the options of the menu in a single arrange
    # The key 'item' is used to specify the quantity asked for user, so it must be initialized in 0 

    menu = [
        {'item':'0', 'name':'Chicken Strips', 'prize':'3.50'},
        {'item':'0', 'name':'French Fries', 'prize':'2.50'},
        {'item':'0', 'name':'Hamburger', 'prize':'4.00'},
        {'item':'0', 'name':'Hotdog', 'prize':'3.50'},
        {'item':'0', 'name':'Large Drink', 'prize':'1.75'},
        {'item':'0', 'name':'Medium Drink', 'prize':'1.50'},
        {'item':'0', 'name':'Milk Shake', 'prize':'2.25'},
        {'item':'0', 'name':'Salad', 'prize':'3.75'},
        {'item':'0', 'name':'Small Drink', 'prize':'1.25'}
    ]

#Initializing the following variables
    i = 0
    precio = 0
    cantidad = 0

# Starting the program   
    print("""
    Menu:
    i. Chicken Strips - $3.50
    ii. French Fries - $2.50
    iii. Hamburger - $4.00
    iv. Hotdog - $3.50
    v. Large Drink - $1.75
    vi. Medium Drink - $1.50
    vii. Milk Shake - $2.25
    viii. Salad - $3.75
    ix. Small Drink - $1.25

    Welcome to our restaurant! Please take a look to our menu, 
    then select all that you want to buy and we wil process your order:""")

    order=input()

    print("You have asked for:")

    if (order.isdigit() == False): # Program will exit if user enters other value than numbers
        print("You can only enter numbers, rebooting...")
    else:
        for i in range(0,len(order)): # it will read all of the values of the string
            if (int(order[i]) == 0): 
                pass # if the user writes 0, the program will skip it
            else:
                precio = float(menu[int(order[i])-1].get("prize")) + precio # It will add the selected prizes of the menu selection
                cantidad = int(menu[int(order[i])-1].get("item")) + 1 # It will take note if a product is ordered twice
                menu[(int(order[i])-1)]["item"] = cantidad # in will change the value of the quantity

        for i in range(0,9): # it will show what the client ordered
            if (int(menu[i].get("item")) != 0): # only will show items which quantity is 1 or more
                print("Quantity",menu[i].get("item"),"Food",menu[i].get("name"))

        print('The prize of the order is: ', precio) # it will print the final prize of the order


        while(1):
            yes_or_no=input('Do you wish to ask for another order? yes/no: ')
            yes_or_no=yes_or_no.lower()
            if (yes_or_no == 'yes'):
                break
            elif (yes_or_no == 'no'):
                sys.exit()
            else:
                print('invalid input, try again')


